# Ansible Handlers #

When developing [ansible role](http://docs.ansible.com/ansible/latest/playbooks_reuse_roles.html) 
you can also create listeners performing some tasks based on some events manually triggered during the 
playbook running. These listeners are called [ansible handlers](http://docs.ansible.com/ansible/latest/playbooks_keywords.html#term-handlers)
and are superhandy when you need to perform some after installation tasks like launching 
just installed particular server. Let's show an example:

**Task:** Create two ansible roles with names "install_nginx" and "start_nginx". First role will install
the nginx server through the ansible shell module via the brew MacOS installer. Second role will containt just
handler with name start_nginx launching the nginx service. **This handler will be triggered from the first role.**

**Solution:**

Howto install nginx? We're going to use ansible shell module and simple MacOS's brew installer.

**..roles/install_nginx/tasks/main.yml:**

	- name: check_install_nginx
  		shell: brew list | grep nginx
  		ignore_errors: true
  		register: is_installed

	- name: install_nginx
  		shell: brew install nginx
  		when: is_installed.rc > 0
  		notify:
    		- start_nginx

	- debug: msg="{{ is_installed.stdout }}"

First task "check_install_nginx" checks whether nginx is already installed.  
Task "install_nginx" is runned only when nginx does not exists yet on the target machine.
If it doesn't then brew installer is invoked and after the installation end **handler start_nginx 
is notified and runned**. Of course, to invoke handler in another role, you need to include dependency 
on the role containing the handler:

**..roles/install_nginx/meta/main.yml**

	dependencies:
   		- { role: start_nginx }                                                                                                                                                                                     

Finally, notified handler "start_nginx" launching the installed nginx:

**..roles/start_nginx/handlers/main.yml**

	- name: start_nginx
  		shell: brew services start nginx

As last thing, to start the nginx installation let's create basic playbook starting the install_nginx role 
which will load start_nginx role before as I showed in the meta directory:

**..launch.yml**

	- hosts: localhost
  		gather_facts: false
  		roles:
    		- install_nginx

And that's it. Let's the test the whole package. 
First run, nginx installation:

**tomask79:handlers tomask79$ ansible-playbook launch.yml**

	PLAY [localhost] ********************************************************************************************************************************************************************

	TASK [install_nginx : check_install_nginx] ******************************************************************************************************************************************
	fatal: [localhost]: FAILED! => {"changed": true, "cmd": "brew list | grep nginx", "delta": "0:00:00.733398", "end": "2018-03-25 22:11:44.838537", "msg": "non-zero return code", "rc": 1, "start": "2018-03-25 22:11:44.105139", "stderr": "", "stderr_lines": [], "stdout": "", "stdout_lines": []}
	...ignoring

	TASK [install_nginx : install_nginx] ************************************************************************************************************************************************
	changed: [localhost]

	TASK [install_nginx : debug] ********************************************************************************************************************************************************
	ok: [localhost] => {
    	"msg": ""
	}

	RUNNING HANDLER [start_nginx : start_nginx] *****************************************************************************************************************************************
	changed: [localhost]

	PLAY RECAP **************************************************************************************************************************************************************************
	localhost                  : ok=4    changed=3    unreachable=0    failed=0   

Nginx was installed, handler was launched. Nice! Let's try second run of the same start command:

	PLAY [localhost] ********************************************************************************************************************************************************************

	TASK [install_nginx : check_install_nginx] ******************************************************************************************************************************************
	changed: [localhost]

	TASK [install_nginx : install_nginx] ************************************************************************************************************************************************
	skipping: [localhost]

	TASK [install_nginx : debug] ********************************************************************************************************************************************************
	ok: [localhost] => {
    	"msg": "nginx"
	}

	PLAY RECAP **************************************************************************************************************************************************************************
	localhost                  : ok=2    changed=1    unreachable=0    failed=0   

No second installation, no handler running. Exactly what we wanted.
To verify that nginx was installed and it's actually running we're going to hit http://localhost:8080

**tomask79:handlers tomask79$ curl http://localhost:8080**

	<!DOCTYPE html>
	<html>
	<head>
	<title>Welcome to nginx!</title>
	<style>
    	body {
        	width: 35em;
        	margin: 0 auto;
        	font-family: Tahoma, Verdana, Arial, sans-serif;
    	}
	</style>
	</head>
	<body>
	<h1>Welcome to nginx!</h1>
	<p>If you see this page, the nginx web server is successfully installed and
	working. Further configuration is required.</p>

	<p>For online documentation and support please refer to
	<a href="http://nginx.org/">nginx.org</a>.<br/>
	Commercial support is available at
	<a href="http://nginx.com/">nginx.com</a>.</p>

	<p><em>Thank you for using nginx.</em></p>
	</body>
	</html>
	tomask79:handlers tomask79$ 

Cheers and have fun with ansible..:-)

regards

Tomas
